export enum Method {
  Get = 'GET',
  Post = 'POST',
  Head = 'HEAD',
  Trace = 'TRACE',
  Patch = 'PATCH',
  Delete = 'DELETE',
  Connect = 'CONNECT',
  Options = 'OPTIONS',
}