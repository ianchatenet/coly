import { Method } from './method.enum';
import { Handler } from '../handler.type';
import { Middleware } from '../middleware/middleware';
import { ALL_STATUS, RouteOptionResponse, RouteOptions } from './routeOptions.type';

/**
 * # Route
 * 
 * This return a Route instance.
 * 
 * ## Example
 * ```js
 * const { Route } = require('coly');
 * 
 * function routeHandler(req, res) {
 *  return 'test'
 * }
 * 
 * const route = new Route({
 *  path: '/test', // takes a string or a regex.
 *  method: 'POST', // takes the enum Method from coly or a valid http method in uppercase.
 *  handler: routeHandler, // takes a function.
 *  response: { 
 *    status: 200, 
 *    contentType: 'text/html'
 *  },
 *  middlewares: [] // takes an array of instanciated Middlewares.
 * });
 * ```
 */
export class Route {

  path: RegExp;
  method: Method;
  response: RouteOptionResponse;
  middlewares: Middleware[] = [];

  handler: Handler;

  constructor(options: RouteOptions) {
    const { path, method, handler, response, middlewares } = { ...options };

    if (typeof path === 'string') {
      this.path = RegExp(path + '$');
    } else if (path instanceof RegExp) {
      this.path = path;
    } else throw new Error('Route need a valid path.');

    if (Object.values(Method).includes(method) === false) throw new Error('Route need a valid http method.');
    if (handler === undefined || handler === null || typeof handler !== 'function') throw new Error('Route need a valid handler.');
    if (ALL_STATUS.includes(response.status) === false) throw new Error('Route need a valid response status.');
    if (response.contentType === undefined || response.contentType === null) throw new Error('Route need a content-type.');

    if (middlewares) {
      if (Array.isArray(middlewares) === false) throw new Error('Option middlewares must be an array.');
      for (let i = 0; i < middlewares.length; i++) {
        const middleware = middlewares[i];
        if (middleware instanceof Middleware === false) throw new Error('A middleware must be a Middleware instance.');
      }

      this.middlewares = middlewares;
    }

    this.method = method;
    this.handler = handler;
    this.response = response;
  }
}