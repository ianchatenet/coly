import { Method } from './method.enum';
import { Middleware } from '../middleware/middleware';
import { Handler } from '../handler.type';

export type RouteOptions = {
  path: string | RegExp,
  method: Method,
  handler: Handler,
  response: RouteOptionResponse,
  middlewares?: Middleware[]
}

export type RouteOptionResponse = { status: ResponseStatus, contentType: string };

// use the array ALL_STATUS to create an union type
export type ResponseStatus = typeof ALL_STATUS[number];

const informationStatus = [
  100,
  101,
  102,
  103
] as const;

const successStatus = [
  200,
  201,
  202,
  203,
  204,
  205,
  206,
  207,
  208,
  226
] as const;

const redirectionStatus = [
  300,
  301,
  302,
  303,
  305,
  306,
  307,
  308
] as const;

const clientErrorStatus = [
  400,
  401,
  402,
  403,
  404,
  405,
  406,
  407,
  408,
  409,
  410,
  411,
  412,
  413,
  414,
  415,
  416,
  417,
  418,
  421,
  422,
  423,
  424,
  425,
  426,
  428,
  429,
  431,
  451
] as const;

const serverErrorStatus = [
  500,
  501,
  502,
  503,
  504,
  505,
  507,
  508,
  511
] as const;

export const ALL_STATUS = [
  ...informationStatus,
  ...successStatus,
  ...redirectionStatus,
  ...clientErrorStatus,
  ...serverErrorStatus
];