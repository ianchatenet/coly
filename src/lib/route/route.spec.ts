import { Route } from './route';
import { Method } from './method.enum';
import { Handler } from '../handler.type';
import { Middleware } from '../middleware/middleware';
import { ResponseStatus, RouteOptionResponse, RouteOptions } from './routeOptions.type';

describe('Route', () => {

  it('Should return a Route instance', () => {
    // arrange
    const dto: RouteOptions = {
      handler: () => 'test',
      method: Method.Get,
      path: '/',
      response: { status: 200, contentType: 'text/html' }
    };
    // act
    const result = new Route(dto);
    // assert
    expect(result instanceof Route).toStrictEqual(true);
  });

  it('Should return a Route instance with regex as path', () => {
    // arrange
    const dto: RouteOptions = {
      handler: () => 'test',
      method: Method.Get,
      path: new RegExp('^test$'),
      response: { status: 200, contentType: 'text/html' }
    };
    // act
    const result = new Route(dto);
    // assert
    expect(result instanceof Route).toStrictEqual(true);
  });

  it('Should return a Route instance with middleware', () => {
    // arrange
    const middleware = new Middleware(() => 'tst');
    const dto: RouteOptions = {
      handler: () => 'test',
      method: Method.Get,
      path: new RegExp('^test$'),
      response: { status: 200, contentType: 'text/html' },
      middlewares: [middleware]
    };
    // act
    const result = new Route(dto);
    // assert
    expect(result instanceof Route).toStrictEqual(true);
  });

  it('Should throw Route need a valid path.', () => {
    // arrange
    const dto: RouteOptions = {
      handler: () => 'test',
      method: Method.Get,
      path: null as unknown as string,
      response: { status: 200, contentType: 'text/html' }
    };
    // act
    let result: unknown;
    try {
      result = new Route(dto);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('Route need a valid path.'));
  });

  it('Should throw Route need a valid http method.', () => {
    // arrange
    const dto: RouteOptions = {
      handler: () => 'test',
      method: 'not an http method' as unknown as Method,
      path: 'test',
      response: { status: 200, contentType: 'text/html' }
    };
    // act
    let result: unknown;
    try {
      new Route(dto);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('Route need a valid http method.'));
  });

  it('Should throw Route need a valid handler.', () => {
    // arrange
    const dto: RouteOptions = {
      handler: 'not a function' as unknown as Handler,
      method: Method.Get,
      path: 'test',
      response: { status: 200, contentType: 'text/html' }
    };
    // act
    let result: unknown;
    try {
      new Route(dto);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('Route need a valid handler.'));
  });

  it('Should throw Route need a valid response status.', () => {
    // arrange
    const dto: RouteOptions = {
      handler: () => 'test',
      method: Method.Get,
      path: 'test',
      response: { status: 1 as unknown as ResponseStatus, contentType: 'text/html' }
    };
    // act
    let result: unknown;
    try {
      new Route(dto);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('Route need a valid response status.'));
  });

  it('Should throw Option middlewares must be an array.', () => {
    // arrange
    const dto: RouteOptions = {
      handler: () => 'test',
      method: Method.Get,
      path: 'test',
      response: { status: 200, contentType: 'text/html' },
      middlewares: 'fail' as unknown as Middleware[]
    };
    // act
    let result: unknown;
    try {
      new Route(dto);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('Option middlewares must be an array.'));
  });

  it('Should throw A middleware must be a Middleware instance.', () => {
    // arrange
    const dto: RouteOptions = {
      handler: () => 'test',
      method: Method.Get,
      path: 'test',
      response: { status: 200, contentType: 'text/html' },
      middlewares: ['fail'] as unknown as Middleware[]
    };
    // act
    let result: unknown;
    try {
      new Route(dto);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('A middleware must be a Middleware instance.'));
  });

  it('Should throw Route need a content-type.', () => {
    // arrange
    const dto: RouteOptions = {
      handler: () => 'test',
      method: Method.Get,
      path: 'test',
      response: { status: 200 } as unknown as RouteOptionResponse,
    };
    // act
    let result: unknown;
    try {
      new Route(dto);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('Route need a content-type.'));
  });

});
