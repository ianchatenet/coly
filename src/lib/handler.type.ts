import { Request } from './request.type';
import { Response } from './response.type';

export type Handler = (req: Request, res: Response) => any;