import { IncomingMessage } from 'http';

export type Request = IncomingMessage & {
  body?: string;
};