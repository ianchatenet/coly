import { Handler } from '../handler.type';

/**
 * # Middleware
 * 
 * This return a Middleware instance.
 * 
 * Server's middlewares are executed before searching if the requested route exist.
 * 
 * Route's middlewares are executed before execute the route handler.
 * 
 * Middlewares are executed in the order you have added them.
 * 
 * ## Example
 * ```js
 * const { Middleware } = require('coly');
 * 
 * function handler() {
 *  console.log('do something');
 * }
 *
 * const middleware = new coly.Middleware(handler);
 * ```
 */
export class Middleware {

  handler: Handler;

  constructor(handler: Handler) {
    if (handler === undefined || typeof handler !== 'function') throw new Error('A middleware need a handler.');
    this.handler = handler;
  }
}