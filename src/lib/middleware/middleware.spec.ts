import { Handler } from '../handler.type';
import { Middleware } from './middleware';

describe('Middleware', () => {

  it('Should return a Middleware instance', () => {
    // arrange
    // act
    const result = new Middleware(() => 'test');
    // assert
    expect(result instanceof Middleware).toStrictEqual(true);
  });

  it('Should throw an error because A middleware need a handler.', () => {
    // arrange
    // act
    let result: unknown;
    try {
      new Middleware('test' as unknown as Handler);
    } catch (error) {
      result = error;
    }
    // assert
    expect(result).toStrictEqual(new Error('A middleware need a handler.'));
  });

});