export type ServerType = 'http';

import http from 'node:http';
export type HttpModule = typeof http;