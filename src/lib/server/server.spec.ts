import { Socket } from 'node:net';
import { IncomingMessage } from 'node:http';

import { Server } from './server';
import { Route } from '../route/route';
import { Request } from '../request.type';
import { Response } from '../response.type';
import { Method } from '../route/method.enum';
import { Middleware } from '../middleware/middleware';
import { RouteOptions } from '../route/routeOptions.type';

describe('Server', () => {

  it('Should return a Server instance using the node:http module', () => {
    // arrange
    // act
    const result = new Server('http');
    // assert
    expect(result instanceof Server).toStrictEqual(true);
  });

  describe('#listen', () => {

    it('Should create a Server call handleRequest methods and listen on port 8000', async () => {
      // arrange
      const req = {} as unknown as Request;
      const res = {} as unknown as Response;

      const listen = jest.fn();
      const mockCallback = (cb: typeof server['handleRequest']) => {
        cb(req, res);
        return ({ listen: listen });
      };
      const createServer = jest.fn(mockCallback);

      jest.mock('http', () => ({
        createServer: createServer,
      }));

      const server = new Server('http');

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const spyHandleRequest = jest.spyOn(server as any, 'handleRequest');
      spyHandleRequest.mockImplementationOnce(jest.fn());
      // act
      await server.listen('8000');
      // assert
      expect(createServer).toHaveBeenCalledTimes(1);
      expect(listen).toHaveBeenNthCalledWith(1, '8000');
      expect(spyHandleRequest).toHaveBeenNthCalledWith(1, req, res);
    });

  });

  describe('#addMiddleware', () => {
    it('Should add a middleware to the server instance', () => {
      // arrange
      const server = new Server('http');
      const middleware = new Middleware(() => 'tst');
      // act
      server.addMiddleware(middleware);
      // assert
      expect(server['middlewares'].length).toStrictEqual(1);
      expect(server['middlewares'][0]).toStrictEqual(middleware);
    });

    it('Should throw "Method addMidleware only takes a Middleware\'s instance"', () => {
      // arrange
      const server = new Server('http');
      // act
      let result: unknown;
      try {
        server.addMiddleware({} as unknown as Middleware);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error('Method addMidleware only takes a Middleware\'s instance'));
    });
  });

  describe('#addRoute', () => {

    it('Should a route to the server instance', () => {
      // arrange
      const dto: RouteOptions = {
        handler: () => 'test',
        method: Method.Get,
        path: new RegExp('^test$'),
        response: { status: 200, contentType: 'text/html' }
      };
      const route = new Route(dto);
      // act
      const server = new Server('http');
      // act
      server.addRoute(route);
      // assert
      expect(server['routes'].length).toStrictEqual(1);
      expect(server['routes'][0]).toStrictEqual(route);
    });

    it('Should throw "Method addRoute only takes a Route\'s instance"', () => {
      // arrange
      // act
      const server = new Server('http');
      // act
      let result: unknown;
      try {
        server.addRoute({} as unknown as Route);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error('Method addRoute only takes a Route\'s instance'));
    });

    it('Should throw "Path \'/^test$/\' already used"', () => {
      // arrange
      // act
      const dto: RouteOptions = {
        handler: () => 'test',
        method: Method.Get,
        path: new RegExp('^test$'),
        response: { status: 200, contentType: 'text/html' }
      };
      const route = new Route(dto);
      const server = new Server('http');
      // act
      server.addRoute(route);
      let result: unknown;
      try {
        server.addRoute(route);
      } catch (error) {
        result = error;
      }
      // assert
      expect(result).toStrictEqual(new Error('Path \'/^test$/\' already used'));
    });
  });

  describe('#handleRequest', () => {

    it('Should listen for data and end events on the request', async () => {
      // arrange
      const req = {
        on: () => jest.fn()
      } as unknown as Request;
      const res = {} as unknown as Response;
      const server = new Server('http');

      const spyOn = jest.spyOn(req, 'on');
      // act
      await server['handleRequest'](req, res);
      // assert
      expect(spyOn).toHaveBeenNthCalledWith(1, 'error', expect.any(Function));
      expect(spyOn).toHaveBeenNthCalledWith(2, 'data', expect.any(Function));
      expect(spyOn).toHaveBeenNthCalledWith(3, 'end', expect.any(Function));
    });

    it('Should concatenate the datas on "error" event', async () => {
      // arrange
      const req = new IncomingMessage(new Socket());
      const res = {
        writeHead: () => jest.fn(),
        write: () => jest.fn(),
        end: () => jest.fn()
      } as unknown as Response;
      const server = new Server('http');

      const spyWriteHead = jest.spyOn(res, 'writeHead');
      const spyWrite = jest.spyOn(res, 'write');

      const error = new Error('fail');
      const spyOn = jest.spyOn(req, 'on');
      // act
      await server['handleRequest'](req, res);
      req.emit('error', error); // simulate a error event
      // assert
      expect(spyOn).toHaveBeenNthCalledWith(1, 'error', expect.any(Function));
      expect(spyOn).toHaveBeenNthCalledWith(2, 'data', expect.any(Function));
      expect(spyOn).toHaveBeenNthCalledWith(3, 'end', expect.any(Function));
      expect(spyWriteHead).toHaveBeenNthCalledWith(1, 500);
      expect(spyWrite).toHaveBeenNthCalledWith(1, `Internal server error\n${error}`);
    });

    it('Should concatenate the datas on "datas" event', async () => {
      // arrange
      const req = new IncomingMessage(new Socket());
      const res = {} as unknown as Response;
      const server = new Server('http');

      const spyOn = jest.spyOn(req, 'on');
      // act
      await server['handleRequest'](req, res);
      req.emit('data'); // simulate a data event
      // assert
      expect(spyOn).toHaveBeenNthCalledWith(1, 'error', expect.any(Function));
      expect(spyOn).toHaveBeenNthCalledWith(2, 'data', expect.any(Function));
      expect(spyOn).toHaveBeenNthCalledWith(3, 'end', expect.any(Function));
    });

    it('Should concatenate the datas on "end" event', async () => {
      // arrange
      const req = new IncomingMessage(new Socket());
      const res = {} as unknown as Response;
      const server = new Server('http');

      const spyOn = jest.spyOn(req, 'on');

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const spyHandleEnd = jest.spyOn(server as any, 'handleEnd');
      spyHandleEnd.mockImplementationOnce(jest.fn());
      // act
      await server['handleRequest'](req, res);
      req.emit('end'); // simulate a data event
      // assert
      expect(spyOn).toHaveBeenNthCalledWith(1, 'error', expect.any(Function));
      expect(spyOn).toHaveBeenNthCalledWith(2, 'data', expect.any(Function));
      expect(spyOn).toHaveBeenNthCalledWith(3, 'end', expect.any(Function));
      expect(spyHandleEnd).toHaveBeenNthCalledWith(1, req, res, []);
    });

  });

  describe('#handleEnd', () => {

    it('Should call writeHead, write and end from response if server has no Routes', async () => {
      // arrange
      const req = {} as unknown as Request;
      const res = {
        writeHead: () => jest.fn(),
        write: () => jest.fn(),
        end: () => jest.fn(),
        url: '/test',
        method: Method.Post,
      } as unknown as Response;
      const spyWriteHead = jest.spyOn(res, 'writeHead');
      const spyWrite = jest.spyOn(res, 'write');
      const spyEnd = jest.spyOn(res, 'end');

      const server = new Server('http');
      // act
      await server['handleEnd'](req, res, []);
      // assert
      expect(spyWriteHead).toHaveBeenNthCalledWith(1, 404);
      expect(spyWrite).toHaveBeenNthCalledWith(1, 'ERROR 404');
      expect(spyEnd).toHaveBeenNthCalledWith(1);
    });

    it('Should call writeHead, write and end from response for a Route that had datas in the request', async () => {
      // arrange
      const req = {
        url: '/test',
        method: Method.Post,
      } as unknown as Request;

      const res = {
        writeHead: () => jest.fn(),
        write: () => jest.fn(),
        end: () => jest.fn()
      } as unknown as Response;

      const spyWriteHead = jest.spyOn(res, 'writeHead');
      const spyWrite = jest.spyOn(res, 'write');
      const spyEnd = jest.spyOn(res, 'end');

      const response = 'test';
      const routeDto: RouteOptions = {
        handler: () => response,
        method: req.method as Method,
        path: req.url as string,
        response: { status: 200, contentType: 'application/json' }
      };
      const route = new Route(routeDto);

      const server = new Server('http');
      server.addRoute(route);
      // act
      await server['handleEnd'](req, res, []);
      // assert
      expect(spyWriteHead).toHaveBeenNthCalledWith(1, routeDto.response.status, { 'Content-Type': routeDto.response.contentType });
      expect(spyWrite).toHaveBeenNthCalledWith(1, response);
      expect(spyEnd).toHaveBeenNthCalledWith(1);
    });

    it('Should call Route\'s handler, and Middleware\'s handler', async () => {
      // arrange
      const req = {
        url: '/test',
        method: Method.Post,
      } as unknown as Request;

      const res = {
        writeHead: () => jest.fn(),
        write: () => jest.fn(),
        end: () => jest.fn()
      } as unknown as Response;

      const middleware = new Middleware(() => 'tst');

      const routeDto: RouteOptions = {
        handler: () => 'test',
        method: req.method as Method,
        path: req.url as string,
        response: { status: 200, contentType: 'application/json' },
        middlewares: [middleware]
      };
      const route = new Route(routeDto);

      const server = new Server('http');
      server.addRoute(route);
      server.addMiddleware(middleware);

      const spyRouteHandler = jest.spyOn(route, 'handler');
      const spyMiddlewareHandler = jest.spyOn(middleware, 'handler');
      // act
      await server['handleEnd'](req, res, []);
      // assert
      expect(spyRouteHandler).toHaveBeenNthCalledWith(1, req, res);
      expect(spyMiddlewareHandler).toHaveBeenNthCalledWith(1, req, res);
      expect(spyMiddlewareHandler).toHaveBeenNthCalledWith(2, req, res);
    });

    it('Should concatenate the datas and set the request body with it', async () => {
      // arrange
      const req = {
        url: '/test',
        method: Method.Post,
      } as unknown as Request;

      const res = {
        writeHead: () => jest.fn(),
        write: () => jest.fn(),
        end: () => jest.fn()
      } as unknown as Response;

      const response = 'test';
      const routeDto: RouteOptions = {
        handler: () => response,
        method: req.method as Method,
        path: req.url as string,
        response: { status: 200, contentType: 'application/json' }
      };
      const route = new Route(routeDto);

      const server = new Server('http');
      server.addRoute(route);

      const spyRouteHandler = jest.spyOn(route, 'handler');
      const datas = 'datas';
      // act
      await server['handleEnd'](req, res, [Buffer.from(datas, 'utf-8')]);
      // assert
      // the req object is mutated during the handleEnd function
      expect(req.body).toStrictEqual(datas);
      expect(spyRouteHandler).toHaveBeenNthCalledWith(1, req, res);
    });

    it('Should call writeHead, with 500, write with the error message if an error is thrown', async () => {
      // arrange
      const req = {
        url: '/test',
        method: Method.Post,
      } as unknown as Request;

      const res = {
        writeHead: () => jest.fn(),
        write: () => jest.fn(),
        end: () => jest.fn()
      } as unknown as Response;

      const spyWriteHead = jest.spyOn(res, 'writeHead');
      const spyWrite = jest.spyOn(res, 'write');
      const spyEnd = jest.spyOn(res, 'end');

      const response = 'test';
      const routeDto: RouteOptions = {
        handler: () => response,
        method: req.method as Method,
        path: req.url as string,
        response: { status: 200, contentType: 'application/json' }
      };
      const route = new Route(routeDto);

      const server = new Server('http');
      server.addRoute(route);

      const error = new Error('fail');
      jest.spyOn(route, 'handler').mockRejectedValueOnce(error);

      const datas = 'datas';
      // act
      await server['handleEnd'](req, res, [Buffer.from(datas, 'utf-8')]);
      // assert
      // the req object is mutated during the handleEnd function
      expect(spyWriteHead).toHaveBeenNthCalledWith(1, 500);
      expect(spyWrite).toHaveBeenNthCalledWith(1, error.message);
      expect(spyEnd).toHaveBeenNthCalledWith(1);
    });


    it('Should call writeHead, with 500, write with "internal server error" if something is thrown', async () => {
      // arrange
      const req = {
        url: '/test',
        method: Method.Post,
      } as unknown as Request;

      const res = {
        writeHead: () => jest.fn(),
        write: () => jest.fn(),
        end: () => jest.fn()
      } as unknown as Response;

      const spyWriteHead = jest.spyOn(res, 'writeHead');
      const spyWrite = jest.spyOn(res, 'write');
      const spyEnd = jest.spyOn(res, 'end');

      const response = 'test';
      const routeDto: RouteOptions = {
        handler: () => response,
        method: req.method as Method,
        path: req.url as string,
        response: { status: 200, contentType: 'application/json' }
      };
      const route = new Route(routeDto);

      const server = new Server('http');
      server.addRoute(route);

      jest.spyOn(route, 'handler').mockRejectedValueOnce('fail');

      const datas = 'datas';
      // act
      await server['handleEnd'](req, res, [Buffer.from(datas, 'utf-8')]);
      // assert
      // the req object is mutated during the handleEnd function
      expect(spyWriteHead).toHaveBeenNthCalledWith(1, 500);
      expect(spyWrite).toHaveBeenNthCalledWith(1, 'Internal server error');
      expect(spyEnd).toHaveBeenNthCalledWith(1);
    });
  });
});