import { Route } from '../route/route';
import { Request } from '../request.type';
import { Response } from '../response.type';
import { Middleware } from '../middleware/middleware';
import { HttpModule, ServerType } from './serverType.enum';

/**
 * # Server
 * 
 * This return a Server instance.
 * 
 * ## Example
 * ```js
 * const coly = require('coly);
 * const server = new coly.Server('http');
 * ```
 */
export class Server {

  private servType: ServerType;
  private routes: Route[];
  private middlewares: Middleware[];


  constructor(servType: ServerType) {
    this.routes = [];
    this.middlewares = [];
    this.servType = servType;
  }

  /**
   * # listen
   * Start the server, listening to the specified port.
   * ## Example
    * ```js
    * server.listen('8000');
    * ```
   */
  async listen(port: string) {
    const serv: HttpModule = await import(this.servType);

    serv.createServer(async (req, res) => {
      await this.handleRequest(req, res);
    }).listen(port);
  }

  /**
   * # addMiddleware
   * Add a middleware for the whole server.
   * 
   * Take a Middleware instance as parameter.
   * ## Example
    * ```js
    * server.addMiddleware(myMiddleware);
    * ```
   */
  addMiddleware(middleware: Middleware) {
    if (middleware instanceof Middleware === false) throw new Error('Method addMidleware only takes a Middleware\'s instance');
    this.middlewares.push(middleware);
  }

  /**
   * # addRoute
   * Add a middleware for the whole server.
   * 
   * Take a Route instance as parameter.
   * ## Example
    * ```js
    * server.addRoute(myRoute);
    * ```
   */
  addRoute(route: Route) {
    if (route instanceof Route === false) throw new Error('Method addRoute only takes a Route\'s instance');
    if (this.routes.some((elmt: Route) => elmt.path === route.path)) throw new Error(`Path '${route.path}' already used`);
    this.routes.push(route);
  }

  private async handleRequest(req: Request, res: Response) {
    const datas: any[] = [];

    req.on('error', (err: Error) => {
      res.writeHead(500);
      res.write(`Internal server error\n${err}`);
      return;
    });

    req.on('data', (chunk) => {
      datas.push(chunk);
    });

    req.on('end', async () => {
      await this.handleEnd(req, res, datas);
    });

  }

  private async handleEnd(req: Request, res: Response, datas: any[]) {
    try {

      if (datas.length > 0) {
        req.body = Buffer.concat(datas).toString();
      }

      for (let i = 0; i < this.routes.length; i++) {
        const route = this.routes[i];

        if (req.url && route.path.test(req.url) === true && route.method === req.method) {

          for (let i = 0; i < this.middlewares.length; i++) {
            const middleware = this.middlewares[i];
            middleware.handler(req, res);
          }

          for (let i = 0; i < route.middlewares.length; i++) {
            const middleware = route.middlewares[i];
            middleware.handler(req, res);
          }

          const response = await route.handler(req, res);
          res.writeHead(route.response.status, { 'Content-Type': route.response.contentType });

          // only write resonse if it exists
          if (response) {
            res.write(response);
          }

          res.end();
          return;
        }
      }
      res.writeHead(404);
      res.write('ERROR 404');
      res.end();
    } catch (error) {

      res.writeHead(500);

      if (error instanceof Error) {
        res.write(error.message);
      } else {
        res.write('Internal server error');
      }

      res.end();
    }
  }
}