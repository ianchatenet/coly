import { ServerResponse } from 'node:http';

export type Response = ServerResponse;