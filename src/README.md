Coly is a web framework built for my own fun.

# Documentation


## Server

This return a Server instance.
Only support **http** for the moment.

### Example
```js
const { Server } = require('coly');
const server = new Server('http');
```

## Route

This return a Route instance.

### Example
```js
const { Route } = require('coly');

function routeHandler(req, res) {
 return 'test'
}

const route = new Route({
 path: '/test', // takes a string or a regex.
 method: 'POST', // takes the enum Method from coly or a valid http method in uppercase.
 handler: routeHandler, // takes a function.
 response: { 
   status: 200, 
   contentType: 'text/html'
 },
 middlewares: [] // takes an array of instanciated Middlewares.
});
```

## Middleware

This return a **Middleware** instance.
Server's middlewares are executed **before searching** if the requested route exist.
Route's middlewares are executed **before executing** the requested route handler.
Middlewares are executed in the **order you have added them**.

### Example
```js
const { Middleware } = require('coly');

function handler() {
 console.log('do something');
}

const middleware = new coly.Middleware(handler);
```