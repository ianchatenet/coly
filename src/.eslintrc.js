module.exports = {
  root: true,
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  env: {
    node: true
  },
  rules: {
    'no-var': 'error',
    'no-return-await': 'error',
    'no-ternary': 'error',
    'prefer-const': 'error',
    'array-bracket-newline': ['error', { 'multiline': true, 'minItems': 3 }],
    'array-element-newline': ['error', { 'multiline': true, 'minItems': 3 }],
    'array-bracket-spacing': ['error', 'always', { 'singleValue': false }],
    eqeqeq: 'error',
    'no-console': 'error',
    'max-nested-callbacks': ['error', 3],
    quotes: ['error', 'single', {
      'avoidEscape': true
    }],
    'max-params': ['error', 3],
    'no-return-await': 'error',
    'semi': ['error', 'always'],
    'no-fallthrough': 'error',
    '@typescript-eslint/naming-convention': [
      'error',
      {
        'selector': 'variable',
        'format': ['camelCase']
      },
      {
        'selector': 'variable',
        'modifiers': ['const'],
        'format': ['camelCase', 'UPPER_CASE']
      },
      {
        'selector': 'typeLike',
        'format': ['PascalCase']
      },
      {
        'selector': 'class',
        'format': ['PascalCase']
      },
      {
        'selector': 'interface',
        'format': ['PascalCase'],
        'custom': {
          'regex': '^I[A-Z]',
          'match': false
        }
      },
    ],
    'max-len': [
      'warn',
      {
        'code': 100,
        'tabWidth': 2,
        'comments': 100,
        'ignoreComments': false,
        'ignoreTrailingComments': true,
        'ignoreUrls': true,
        'ignoreStrings': true,
        'ignoreTemplateLiterals': true,
        'ignoreRegExpLiterals': true
      }
    ]
  }
};