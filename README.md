Coly is a web framework built for my own fun.

[Library documentation](./src/README.md)

[Examples](./examples/README.md)