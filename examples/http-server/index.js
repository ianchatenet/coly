
const { Route } = require('@ic/coly');
const { Server } = require('@ic/coly');
const { Middleware } = require('@ic/coly');

const server = new Server('http');

function routeHandler(req, res) {
  return 'example';
}

const route = new Route({
  path: '/example', // takes a string or a regex.
  method: 'POST', // takes the enum Method from coly or a valid http method in uppercase.
  handler: routeHandler, // takes a function.
  response: {
    status: 200,
    contentType: 'text/html'
  },
  middlewares: [] // takes an array of instanciated Middlewares.
});

function midHandler(req, res) {
  console.log('example');
}

const middleware = new Middleware(midHandler);

server.addRoute(route);
server.addMiddleware(middleware);

server.listen('8000');